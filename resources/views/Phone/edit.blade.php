{!! Html::style('css/bootstrap.min.css') !!}
@foreach($errors->all() as $error)
    {!! $error !!}
@endforeach

<section>
    <div class="container">
        <h3 class="text-center">Phone App</h3>
        <hr>
        <div class="row">

            <div class="panel panel-default">
                <div class="panel-heading text-center"><h4>Edit Your Number</h4></div>
                <div class="panel-body">
                    {!! Form::model($phone,['method'=>'PATCH', 'route'=>['phone.update', $phone->id],'class'=>'form-horizontal']) !!}

                    <div class="form-group">
                        {!! Form::label('name','Your Name : ',array('class' =>'col-sm-2 control-label')) !!}
                        <div class="col-sm-10">
                            {!! Form::text('name', null,
                                array('required',
                                      'class'=>'form-control')) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        {!! Form::label('number','Your Number : ',array('class' =>'col-sm-2 control-label')) !!}
                        <div class="col-sm-10">
                            {!! Form::number('number', null,
                                array('required',
                                      'class'=>'form-control')) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            {!! Form::submit('Edit',
                              array('class'=>'btn btn-md btn-primary ')) !!}
                            <a href="/phone"  class="btn btn-default text-center"><span class="glyphicon glyphicon-list-alt"></span>  Go to List</a>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>

            </div>
        </div>
</section>
