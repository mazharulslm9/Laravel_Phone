{!! Html::style('css/bootstrap.min.css') !!}
<section>
    <div class="container">
        <h3 class="text-center">Phone App</h3>
        <hr>
        <div class="row">

            <div class="panel panel-default">
                <div class="panel-heading text-center"><h4>Contact Details</h4> </div>
                <div class="panel-body">
                    <div class="panel panel-default">
                        <div style="background-color: #1b6d85;color: #FFFFFF;font-size: 18px;" class="panel-heading bg-primary">Name</div>
                        <div style="background-color: #d5d5d5" class="panel-body">{!! $phone->name !!}</div>
                        <div style="background-color: #4cae4c;color: #FFFFFF;font-size: 18px;" class="panel-heading bg-info">Number</div>
                        <div style="background-color: #c7ddef" class="panel-body">{!! $phone->number !!}</div>
                    </div>
                    <a class="btn btn-md btn-danger" href="javascript:history.go(-1)">Back</a>
                </div>

            </div>

        </div>

    </div>


</section>

