{!! Html::style('css/bootstrap.min.css') !!}
<style>
    table {
        margin: 10px auto;
        text-align: center ;
    }
    .delete {
        display: inline-block;
    }
</style>

    @if(Session::has('flash_message'))
        <div class="alert alert-success"><em>{{ Session::get('flash_message')}}</em></div>
    @endif

<section >
    <div class="container">
        <h3 class="text-center">Phone Numbers List</h3>
        <hr>
        <div class="row">
          <a class="btn btn-md btn-success text-center" href="{!! url('phone/create') !!}"> <span class="glyphicon glyphicon-plus"></span>  Add New</a>
            <a href="/phone"  class="btn btn-default text-center"><span class="glyphicon glyphicon-list-alt"></span>  Go to List</a>
            <div class="table-responsive">
            <table  class="table table-bordered table-striped">
                <thead>
                    <td>SL No</td>
                    <td>Name</td>
                    <td>Number</td>
                    <td >Action</td>
                </thead>
                <?php $i=1; ?>
                @foreach($phones as $phone)
                    <tbody>

                    <tr>
                        <td>{{$i++}}</td>
                        <td>{!! $phone->name !!}</td>
                        <td>{!! $phone->number !!}</td>
                        <td>
                            <a class="btn btn-md btn-primary" href="{!! url('phone/'.$phone->id.'/edit') !!}"><span class="glyphicon glyphicon-pencil"></span>  Edit</a>
                            <a class="btn btn-md btn-info" href="{!! url('phone/'.$phone->id) !!}"><span class="glyphicon glyphicon-eye-open"></span>  View</a>
                            {!! Form::open(array('method'=>'DELETE','route'=>array('phone.destroy',$phone->id),'class'=>'delete')) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-eye-open"></span>  Delete', array('class' => 'btn btn-md btn-danger','type'=>'submit')) !!}
                            {!! Form::close() !!}

                        </td>
                    </tr>
                @endforeach
                    </tbody>
            </table>
            </div>
            <div class="text-center">
                {{ $phones->links() }}
            </div>


        </div>

    </div>
</section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script>
    $('div.alert').delay(2000).slideUp(3000);
</script>